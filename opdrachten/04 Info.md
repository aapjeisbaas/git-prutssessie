# Info
Een van de mooie dingen van git, is dat, terwijl je eigenlijk bezig bent om controle te krijgen en houden 
over het ontwikkelproces, je ondertussen ook enorm veel informatie genereert.

Een mooi voorbeeld van die informatie is te vinden als je googled op 'visualisation git kernel'.
O.a. onderstaande filmpje staat in de lijst met resultaten:
* Visualisation: https://www.youtube.com/watch?v=5iFnzr73XXk
Het filmpje geeft een visualisati van de ontwikkelingen van de kernel en is tot stand gekomen door de git repo 
van de linux kernel door een visualisati tool te halen.

Naast mooie filmpjes is die informatie ook enorm waardevol, omdat je er enorm goed terug kunt vinden waar bepaalde
onderdelen va je code nu precies vandaan komen. Maar hoe krijg je nu toegang tot die informatie.

# Info commandos
## git diff
Het commando `git diff` geeft een overzicht van de aampassingen in de workdir. De optie --cached (`git diff --cached`) neemt hierbij wijzigingen in de stage locatie ook mee.

## git status
`git status` is het commando waarmee je informatie kunt terug halen over waar een bepaalde 

legio van informatie kunt terug halen over de huidige stand van zaken. Je ziet (indien van toepassing)
* een overzicht van de bestanden zie zijn aangepast,
* een overzicht van de bestanden in je staging area (de bestanden die met de volgende commit mee gaan)
  * Let op. Een bestand kan in beide locaties voorkomen, dan is hij na `git add` weer aangepast. De versie in de staging area gaat met de volgende commit mee.
* een overzicht van bestanden die nog niet in je repo staan (untracked files)
* merge conflicten
Bij de overzichten geeft git tevens een hint at je kunt doen, zoals:
* `git add` (voeg aangepaste bestanden toe aan je staging area
* `git checkout --` (maak aanpassingen op een bestand ongedaan)
* `git reset HEAD` (haal bestanden uit je staging area

Handig om te weten:
* Je kunt bestanden / folders weg halen uit `git status`. Die doen als het ware niet meer mee.
  handig voor bestanden die gegenenreerd worden, zoals binaries en so bestanden die uit make komen, .pyc bestanden, logfiles, etc.
  * met een .gitignore file in de root van je git project. Deze file is onderdeel van je git project en wijzigingen hierop komen dus in `git status` naar voren.
  * met een exclude bestand in de map .git/info/ van je git project. Dit bestand is persoonlijk (gaat niet naar de git remote)

## git log
Het commando `git log` geeft een voleldig verslag van de commits die uiteindelijk leiden tot de HEAD van de gekozen branch (huidige branch als geen barnchenaam wordt meegegeven).

## git show
Het commando `git show` geeft meer informatie over een commit (de commit info die ook in git log wordt opgenomen, alsook de git diff informatie van de commit).
Je kunt de betreffende commit als extra argument meegeven. Zonder commit, kiest hij de laatste commit van de huidige branch.

## git blame
Het commando `git blame` geeft van een file per regel aan, uit welke commit (met naam en datum) de regel als laatste heeft aangepast.
Je kunt dan verdere info over die commit krijgen met `git show`.

# Opdracht 4
* Vraag een overzicht van de commits:
```
git log
```
* Controlleer de inhoud van de laatste commit
```
git show
```
* Vraag alle wijzigingen op van het bestand deelnemers/log.txt
```
git blame deelnemers/log.txt
```
* vraag de inhoud van een van de betreffende commits op de file op.
```
#Bijvoorbeeld
git show e9f25772
```
