# Remotes
Een remote is een andere repository, waar de wijzigingen van je lokale repository naar toe kunnen worden gesynchroniseerd.
In ons voorbeeld hebben we met het clone commando een lokale repository opgebouwd van de remote git@xforce.tilaa.cloud:prutssessie/git_20180321.git.
De lokale repository bevat na de clone alle commits die op de remote beschikbaar zijn en de remote is (onder de naam 'origin;) geregistreerd als de standaard remote.
Daarmee is `git push` en `git pull` automatisch gekoppeld aan deze remote.

Het gebruik van een remote levert vele opties.
* Een remote is makkelijk makkelijk wijzigingen uitwisselen.
  * omdat de remote altijd online is, kunnen developers commits uploaden en downloaden zonder zelf altijd online te hoeven zijn.
  * om commits uit te wisselen hebben de developers geen toegang nodig tot elkaars systemen
* De code is op een extra plek opgeslagen. Prettig als je laptop stuk gaat / weg raakt.
* De remote kan onderdeel zijn van een repo manager. Die kan extra opties bieden, zoals:
  * Onderdeel van een CI/CD pipeline
  * Een review oplossing
  * Integratie met gebruikende systemen
  * Integratie met een issue tracker
  * Makkelijkere authorisatie

Het kan heel praktisch zijn om een extra remote in te stellen.
Bijvoorbeeld een interne gitlab naast een externe (github).
Of een gedeelde repo en een persoonlijke repo.
Meer over het laatste voorbeeld in het volgende hoofdstuk.

Opdrachten:
* Maak een fork van het gedeelde gitlab project naar je persoonlijke gitlab ruimte:
  * Ga naar het gedeelde gitlab project: https://xforce.tilaa.cloud/prutssessie/git_20180321
  * Klik op 'Fork'
  * Klik op je eigen foto om te forken naar de persoonlijke ruimte
  * Pas bij 'settings' / 'General' / 'General project settings' de default branch aan naar 'develop'
  * Hef bij 'settings' / 'Repository' / 'Protected Branches' de master branch de beveiliging op (unprotect)
* Stel je persoonlijk repo in als extra repo met als naam 'mine' (url vind je op je persoonlijke fork).
```
cd ~/gitlab_conclusion/git_20180321
git remote add mine git@xforce.tilaa.cloud:sebastiaanm/git_20180321.git
git pull mine
```
* Controleer welke remotes je hebt geconfigureerd (met -v krijg je ook de url erbij)
```
git remote -v
```
* Stel voor de develop branch je persoonlijk repo in als de standaard upstream remote / repo
```
git checkout develop
git branch -u mine/develop
```
* Doe een aantal aanpassingen, commit ze naar develop en push ze naar je persoonlijke remote
```
for ((i=1; i<5; i++)); do
  echo "$(date): regel $i ingevoegd" >> deelnemers/log.txt ; git commit 'deelnemers/log.txt' -m "Datum $i toegevoegd aan deelnemers/log.txt"; sleep 2; done
done
git push
```
* List een eerdere commit met git log en rol je locale en remote branch terug
```
git log
git reset --hard 328277ad131756b6ebbee00e1d41859c4ecc758c
git push -f
```
* list de remote branches en verwijder de master branch in je persoonlijke remote
```
git branch -a
git push -d mine master
```
